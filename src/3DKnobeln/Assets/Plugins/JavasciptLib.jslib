﻿var LibraryPageTool = {
    GoFullscreen: function()
    {
        var viewFullScreen = document.getElementById('#canvas');

        var ActivateFullscreen = function()
        {
            if (viewFullScreen.requestFullscreen) /* API spec */
            {
                viewFullScreen.requestFullscreen();
            }
            else if (viewFullScreen.mozRequestFullScreen) /* Firefox */
            {
                viewFullScreen.mozRequestFullScreen();
            }
            else if (viewFullScreen.webkitRequestFullscreen) /* Chrome, Safari and Opera */
            {
                viewFullScreen.webkitRequestFullscreen();
            }
            else if (viewFullScreen.msRequestFullscreen) /* IE/Edge */
            {
                viewFullScreen.msRequestFullscreen();
            }

            viewFullScreen.removeEventListener('touchend', ActivateFullscreen);
        }

        viewFullScreen.addEventListener('touchend', ActivateFullscreen, false);
    },
    ExitFullscreen: function() 
    {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) { /* Safari */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE11 */
            document.msExitFullscreen();
        }
        unityInstance.Quit(function() {
            console.log("done!");
        });
        unityInstance = null;
        location.reload();
    },
    GetJSON: function(path, objectName, callback, fallback) {
        var parsedPath = Pointer_stringify(path);
        var parsedObjectName = Pointer_stringify(objectName);
        var parsedCallback = Pointer_stringify(callback);
        var parsedFallback = Pointer_stringify(fallback);
        
        try {
                unityInstance.Module.SendMessage(parsedObjectName, parsedCallback, 
                    "" + leaderboard[0]["name"] + ": " + leaderboard[0]["score"] + "|" +
                    "" + leaderboard[1]["name"] + ": " + leaderboard[1]["score"] + "|" +
                    "" + leaderboard[2]["name"] + ": " + leaderboard[2]["score"]
                );

        } catch (error) {
            unityInstance.Module.SendMessage(parsedObjectName, parsedFallback, JSON.stringify(error, Object.getOwnPropertyNames(error)));
        }
    },
    PostJSON: function(path, value, objectName, callback, fallback) {
        var parsedPath = Pointer_stringify(path);
        var parsedValue = Pointer_stringify(value);
        var parsedObjectName = Pointer_stringify(objectName);
        var parsedCallback = Pointer_stringify(callback);
        var parsedFallback = Pointer_stringify(fallback);

        parsedValue = JSON.parse(parsedValue);
        
        try {

            firebase.database().ref(parsedPath).set(parsedValue).then(function() {
                unityInstance.Module.SendMessage(parsedObjectName, parsedCallback, "Success: " + parsedValue + " was posted to " + parsedPath);
            });

        } catch (error) {
            unityInstance.Module.SendMessage(parsedObjectName, parsedFallback, JSON.stringify(error, Object.getOwnPropertyNames(error)));
        }
    },
};
mergeInto(LibraryManager.library, LibraryPageTool);
