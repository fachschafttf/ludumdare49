﻿var leaderboard = []
var modes = ["adventure", "creative"]


var loadLeaderBoard = function() {
    for (let modeIndex in modes)
    {
        mode = modes[modeIndex]
        firebase.database().ref(mode).orderByChild("score").limitToLast(100).on('value', function (snapshot) {
            leaderboard = [];
            var unsorted = snapshot.val();
            for (var key in unsorted) {
                if (unsorted.hasOwnProperty(key)) {
                    leaderboard.push({"score": unsorted[key]['score'].toString(), "name": unsorted[key]['name']});
                }
            }

            leaderboard.sort(function (a, b) {
                return b["score"] - a["score"];
            });
            var content = ""
            for (var key in leaderboard) {
                content += `<li class="list-group-item d-flex justify-content-between align-items-center">${leaderboard[key]["name"]}<span class="badge bg-primary">${leaderboard[key]["score"]}</span>
    </li>`
            }
            mode = snapshot._delegate.ref._path.pieces_[0]
            document.getElementById("leaderboard-content-" + mode).innerHTML = content;
        });
    }
}

loadLeaderBoard();