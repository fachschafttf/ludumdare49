using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// ReSharper disable once CheckNamespace
public class Overlay : MonoBehaviour, PlayerActions.IIngameOverlayControlsActions
{
    private PlayerActions _controls;
    public GameObject Transparent;
    public GameObject PauseOverlay;
    public GameObject SettingsMenu;
    public GameObject GameOverOverlay;
    public GameObject SubmissionOverlay;
    public Button ResumeButton;
    public Button SubmitButton;

    private GameController _gameController;

    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        _gameController = FindObjectOfType<GameController>();
    }

    public void OnTogglePause(InputAction.CallbackContext context)
    {
        if (!context.performed)
        {
            return;
        }

        TogglePause();
    }

    public void TogglePause()
    {
        if (_gameController.Paused)
        {
            DisablePause();
        }
        else
        {
            EnablePause();
        }
    }

    public void GameOver()
    {
        Transparent.SetActive(true);
        GameOverOverlay.SetActive(true);
        SubmitButton.Select();
    }


    public void Replay()
    {
        SceneManager.LoadScene("MainLevel");
    }

    private void EnablePause()
    {
        _gameController.Paused = true;
        Transparent.SetActive(true);
        PauseOverlay.SetActive(true);
        ResumeButton.Select();
    }

    public void DisablePause()
    {
        _gameController.Paused = false;
        Transparent.SetActive(false);
        PauseOverlay.SetActive(false);
        SettingsMenu.SetActive(false);
    }

    // ReSharper disable once UnusedMember.Global
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnEnable()
    {
        if (_controls == null)
        {
            _controls = new PlayerActions();
            _controls.IngameOverlayControls.SetCallbacks(this);
        }
        _controls.Enable();
    }

    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnDisable()
    {
        _controls.Disable();
    }
}
