using UnityEngine;
using UnityEngine.SceneManagement;

// ReSharper disable once UnusedMember.Global
// ReSharper disable once CheckNamespace
public class MainMenu : MonoBehaviour
{
    // ReSharper disable once UnusedMember.Global
    public void PlayGame(int gameMode)
    {
        if (GamemodeController.Instance)
        {
            GamemodeController.Instance.GameMode = gameMode;
        }
        SceneManager.LoadScene("MainLevel");
    }

    // ReSharper disable once UnusedMember.Global
    public void QuitGame()
    {
        Debug.Log("QuitGame");
        Application.Quit();
    }
}
