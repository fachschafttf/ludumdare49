using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreSetter : MonoBehaviour
{
    public TextMeshProUGUI ScoreField;
    public TMP_InputField InputField;

    private string _placeHolderText = "Anon";
    private string _playerUuid;
    
    // Firebase stuff
    [DllImport("__Internal")]
    public static extern void GetJSON(string path, string objectName, string callback, string fallback);
    
    [DllImport("__Internal")]
    public static extern void PostJSON(string path, string value, string objectName, string callback, string fallback);
    // Start is called before the first frame update
    void Start()
    {
        resetUUID();
        SetInfoOnOverlay();
    }

    public void SendHighScore(string playerName, string mode)
    {
        var obj = new Dictionary<string, dynamic>() {{"score", GameController.Instance.PlayerHighScore}, {"name", $"\"{playerName}\""}};
        var jsonString = MyDictionaryToJson(obj);
        var refPath = mode + "/" + _playerUuid;
        
#if UNITY_EDITOR
        Debug.Log($"Post highscore: {jsonString} to \"{refPath}\"");
#else
                PostJSON(refPath, jsonString, "HighScore", "JSuccess", "fallback");
                Debug.Log($"Post highscore: {jsonString}");
#endif
        
    }

    private static string MyDictionaryToJson(Dictionary<string, dynamic> dict)
    {
        var entries = dict.Select(entry => $"\"{entry.Key}\": {entry.Value}").ToList();
        return "{" + string.Join(",", entries) + "}";
    }

    public void resetUUID()
    {
        _playerUuid = System.Guid.NewGuid().ToString();
    }


    private void SetInfoOnOverlay()
    {
        ScoreField.text = GameController.Instance.PlayerHighScore.ToString();

        if (InputField)
        {
            var playerName = PlayerPrefs.GetString("PlayerName", String.Empty);
            if (!(String.IsNullOrEmpty(playerName)))
            {
                InputField.text = playerName;
            }
        }
    }

    public void SubmitScore()
    {
        var playerName = InputField.text;
        if (playerName == "")
        {
            playerName = "Anon";
        }
        PlayerPrefs.SetString("PlayerName", playerName);
        SendHighScore(playerName, GamemodeController.ModeList[GamemodeController.Instance.GameMode]);
    }
}
