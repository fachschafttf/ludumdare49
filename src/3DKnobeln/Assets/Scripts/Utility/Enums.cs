
// North is looking in positive z (blue), East is looking in positive x (red)
// ReSharper disable once CheckNamespace
public enum Directions {North, East, South, West, Free}
