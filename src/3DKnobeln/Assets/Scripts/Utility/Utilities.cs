
using System;
using UnityEngine;

// Static function for stuff like Modulo, or simple math things

// ReSharper disable once CheckNamespace
public static class Utilities
{
    public static int Mod(int x, int m)
    {
        return (x % m + m) % m;
    }

    public static Vector2 RotateVector2(Vector2 vector, float degree)
    {
        var tempVector = new Vector3(vector.x, 0, vector.y);
        tempVector = Quaternion.AngleAxis(degree, Vector3.up) * tempVector;
        return new Vector2(tempVector.x, tempVector.z);
    }

    public static bool IsAnyCoordinateTooFar(Vector2 vector, float value)
    {
        return Math.Abs(vector.x) > value || Math.Abs(vector.y) > value;
    }
}
