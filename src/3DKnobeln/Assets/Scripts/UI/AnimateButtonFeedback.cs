using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable once CheckNamespace
public class AnimateButtonFeedback : MonoBehaviour
{
    public Image ButtonImg;
    public RectTransform ButtonRectTransform;

    [Space]
    [Header("Animation")]
    [Tooltip("factor for the size"), Range(.25f, 4f)]
    public float Scale = 1.5f;
    [Tooltip("How long the animation should last in total"), Range(.01f, 2f)]
    public float Duration = .1f;
    //[Tooltip("Color for the button while animating")]
    //public Color Color;

    // ReSharper disable once UnusedMember.Local
    private void Start()
    {
        // try to get references if we forgot to add one
        if (ButtonImg == null)
        {
            ButtonImg = GetComponentInChildren<Image>();
        }
        if (ButtonRectTransform == null)
        {
            ButtonRectTransform = GetComponentInChildren<RectTransform>();
        }
    }

    public void PlayButtonPressAnimation()
    {
        // Debug.Log("playing button animation");
        GrowInSize();
        //ChangeColor();
    }

    // ReSharper disable once UnusedMember.Local
    private void ChangeColor()
    {
        // var changer = ColorChange(Color, Duration);
        // StartCoroutine(changer);
    }

    private void GrowInSize()
    {
        var grower = SizeChanger(Scale, Duration);
        StartCoroutine(grower);
    }

    // ReSharper disable once UnusedMember.Local
    IEnumerator ColorChange(Color color, float duration)
    {
        ButtonImg.color += color;
        yield return new WaitForSeconds(duration);
        ButtonImg.color -= color;
    }

    IEnumerator SizeChanger(float scale, float duration)
    {
        ChangeSize(scale);
        yield return new WaitForSeconds(duration);
        ChangeSize(1f / scale);
    }
    private void ChangeSize(float scale)
    {
        ButtonRectTransform.localScale *= scale;
    }

    public void MakeBig()
    {
        ChangeSize(Scale);
    }

    public void MakeSmall()
    {
        ChangeSize(1f / Scale);
    }
}
