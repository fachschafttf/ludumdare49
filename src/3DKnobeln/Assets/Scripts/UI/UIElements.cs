using UnityEngine;
using UnityEngine.EventSystems;

// ReSharper disable once CheckNamespace
// ReSharper disable once InconsistentNaming
public class UIElements : MonoBehaviour, IPointerDownHandler, IPointerUpHandler

{
    private MovementController _movementController;
    private CameraController _cameraController;

    // Start is called before the first frame update
    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        _movementController = FindObjectOfType<MovementController>();
        _cameraController = FindObjectOfType<CameraController>();
    }

    // Update is called once per frame
    // ReSharper disable once UnusedMember.Local
    void Update()
    {
        
    }

    // ReSharper disable once UnusedMember.Global
    public void MoveLeft()
    {
        _movementController.MoveLeft();
        AudioController.Instance.PlaySound(_movementController.SoundMoveLeft);
    }
    
    // ReSharper disable once UnusedMember.Global
    public void MoveRight()
    {
        _movementController.MoveRight();
        AudioController.Instance.PlaySound(_movementController.SoundMoveRight);
    }

    // ReSharper disable once UnusedMember.Global
    public void MoveNear()
    {
        _movementController.MoveNear();
        AudioController.Instance.PlaySound(_movementController.SoundMoveNear);
    }

    // ReSharper disable once UnusedMember.Global
    public void MoveFar()
    {
        _movementController.MoveFar();
        AudioController.Instance.PlaySound(_movementController.SoundMoveFar);
    }

    public void StartFastDown()
    {
        _movementController.StartFastDown();
    }

    public void StopFastDown()
    {
        _movementController.StopFastDown();
    }

    // ReSharper disable once UnusedMember.Global
    public void RotateCameraLeft()
    {
        // Debug.Log("Test");
        _cameraController.RotateCameraLeft();
        AudioController.Instance.PlaySound(_cameraController.SoundCameraLeft);
    }
    
    // ReSharper disable once UnusedMember.Global
    public void RotateCameraRight()
    {
        _cameraController.RotateCameraRight();
        AudioController.Instance.PlaySound(_cameraController.SoundCameraRight);
    }

    // ReSharper disable once UnusedMember.Global
    public void RotateBlockLeft()
    {
        _movementController.RotateLeft();
        AudioController.Instance.PlaySound(_movementController.SoundRotateLeft);
    }
    
    // ReSharper disable once UnusedMember.Global
    public void RotateBlockRight()
    {
        _movementController.RotateRight();
        AudioController.Instance.PlaySound(_movementController.SoundRotateRight);
    }
    
    // ReSharper disable once UnusedMember.Global
    public void RotateBlockUp()
    {
        _movementController.RotateUp();
        AudioController.Instance.PlaySound(_movementController.SoundRotateUp);
    }
    
    // ReSharper disable once UnusedMember.Global
    public void RotateBlockDown()
    {
        _movementController.RotateDown();
        AudioController.Instance.PlaySound(_movementController.SoundRotateDown);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("down");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("up");
    }
}
