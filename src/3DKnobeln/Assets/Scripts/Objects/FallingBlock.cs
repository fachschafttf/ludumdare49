using UnityEngine;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
public class FallingBlock : MonoBehaviour
{
    public GameController GameController;
    private Block _block;
    private GameObject _ghost;

    private bool _isSnappedDown;
    // Start is called before the first frame update
    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        _block = GetComponent<Block>();
        _ghost = new GameObject("Ghost");
        _ghost.gameObject.layer = LayerMask.NameToLayer("Block");
        MeshFilter meshFilter = _ghost.AddComponent<MeshFilter>();
        _ghost.AddComponent<MeshRenderer>();
        meshFilter.sharedMesh = GetComponentInChildren<MeshFilter>().sharedMesh;
        Material ghostMat = Resources.Load("Materials/Ghost", typeof(Material)) as Material;
        
        _ghost.transform.position = transform.parent.transform.position;
        _ghost.transform.rotation = Quaternion.identity;
        Material[] blockMats = _block.GetComponentInChildren<Renderer>().materials;
        Color[] colors = new Color[blockMats.Length];
        Material[] newMats = new Material[colors.Length];
        for (int i = 0; i<colors.Length; i++)
        {
            colors[i] = blockMats[i].GetColor("_EmissionColor");
            colors[i].a = 0.5f;
            // colors[i] *= 0.7f;

            newMats[i] = new Material(ghostMat);
            newMats[i].color = colors[i];
            newMats[i].SetColor("_EmissionColor", colors[i]);
        }
  

        _ghost.GetComponent<Renderer>().materials = newMats;

    }

    // ReSharper disable once UnusedMember.Local
    private void OnDestroy()
    {
        Destroy(_ghost);
        /*
        gameObject.AddComponent<TraumaInducer>();
        gameObject.GetComponent<TraumaInducer>().Delay = 0;
        gameObject.GetComponent<TraumaInducer>().MaximumStress = 0.2f;
        */
        //GetComponent<TraumaInducer>().StartCoroutine();
    }

    // Update is called once per frame
    // ReSharper disable once UnusedMember.Local
    void Update()
    {
        if (_block.DistanceToHighestUnderlyingPoint() < 0.35f || transform.parent.position.y < 0.1)
        {
            GameController.PlaceBlock(Vector3.up * 0.1f);
        }

        // Debug.Log(block.DistanceToHighestUnderlyingPoint());
    }

    // ReSharper disable once UnusedMember.Local
    private void LateUpdate()
    {
            DrawGhost();
    }

    // ReSharper disable once UnusedMember.Local
    void OnCollisionEnter(Collision collision)
    {
        GameController.PlaceBlock(Vector3.up * 0.1f);
        collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        collision.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

    }


    // ReSharper disable once UnusedMember.Local
    // ReSharper disable once UnusedParameter.Local
    private void OnTriggerEnter(Collider other)
    {
        /*
        if (other.gameObject != gameObject && (other.gameObject.layer == LayerMask.NameToLayer("Environment") || other.gameObject.gameObject.layer == LayerMask.NameToLayer("Block")))
        {
            gameController.PlaceBlock(Vector3.up * other.contactOffset * 2);
        }
        */
     
    }

    public void SnapDown()
    {
        _isSnappedDown = true;
    }

    public void DrawGhost()
    {
        if (_ghost == null)
            return;

        float upShift = 0;
        var parentPosition = transform.parent.transform.position;

        foreach (var coll in GetComponentsInChildren<BoxCollider>())
        {
            Bounds bounds = coll.bounds;


            LayerMask mask = LayerMask.GetMask("Environment", "Block");
            RaycastHit hit;
            if (Physics.BoxCast(bounds.center, bounds.extents, Vector3.down, out hit, Quaternion.identity, 400f, mask)) { 
            if (hit.transform.gameObject == gameObject)
                continue;
            float upperPoint = hit.point.y + Mathf.CeilToInt((parentPosition - bounds.min).y - 0.1f);
            if (upperPoint > upShift)
                upShift = upperPoint;
            }

            //DrawLine(bounds.center, hit.point, Color.green);

        }

        _ghost.transform.position = new Vector3(parentPosition.x, upShift, parentPosition.z);
        _ghost.transform.rotation = transform.parent.transform.rotation;
    }

    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.05f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Universal Render Pipeline/Particles/Lit"));
        lr.startColor = lr.endColor = color;
        lr.startWidth = lr.endWidth = 0.1f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }
}
