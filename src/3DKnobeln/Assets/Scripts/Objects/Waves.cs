using System;
using System.Drawing;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

[ExecuteInEditMode]
public class Waves : MonoBehaviour
{
    public int Dimension = 20;
    public float UVScale = 1f;

    protected MeshFilter meshFilter;

    public Octave[] Octaves;

    protected Mesh mesh;
    // Start is called before the first frame update

    void Start()
    {
        mesh = new Mesh();
        mesh.name = gameObject.name;

        mesh.vertices = GenerateVertices();
        mesh.triangles = GenerateTriangles();
        mesh.uv = GenerateUVs();

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();


        meshFilter = gameObject.GetComponent<MeshFilter>();
        if (!meshFilter)
        {
            meshFilter = gameObject.AddComponent<MeshFilter>();
        }
        meshFilter.mesh = mesh;
    }

    private Vector2[] GenerateUVs()
    {
        var uvs = new Vector2[mesh.vertices.Length];
        for (var x = 0; x <= Dimension; x++)
        {
            for (var z = 0; z <= Dimension; z++)
            {
                uvs[index(x, z)] = new Vector2(x / (float)Dimension, z / (float)Dimension);
            }
        }

        return uvs;
    }

    private Vector3 MoveToCenter(Vector3 position)
    {
        return position - new Vector3(Dimension / 2f, 0, Dimension / 2f);
    }


    private Vector3[] GenerateVertices()
    {
        var vertices = new Vector3[(Dimension + 1) * (Dimension + 1)];

        for (var x = 0; x <= Dimension; x++)
        {
            for (var z = 0; z <= Dimension; z++)
            {
                vertices[index(x, z)] = MoveToCenter(new Vector3(x, 0, z));
            }
        }

        return vertices;
    }

    // Returns the current height of the mesh at a specified point.
    public float GetHeightAtPoint(Vector3 position)
    {

        var relativePosition = ConvertToRelativePosition(position);

        var x_Low = (int) Math.Floor(relativePosition.x);
        var z_Low = (int) Math.Floor(relativePosition.z);
        var x_High = (int)Math.Ceiling(relativePosition.x);
        var z_High = (int)Math.Ceiling(relativePosition.z);

        var xGradient = relativePosition.x - x_Low;
        var zGradient = relativePosition.z - z_Low;




        var leftLowerMeshPoint = mesh.vertices[index(x_Low, z_Low)];
        var leftUpperMeshPoint = mesh.vertices[index(x_Low, z_High)];
        var rightUpperMeshPoint = mesh.vertices[index(x_High, z_Low)];
        var rightLowerMeshPoint = mesh.vertices[index(x_High, z_High)];



        var xDeltaHeightBack = rightLowerMeshPoint.y - leftLowerMeshPoint.y;
        var xDeltaHeightFront = rightUpperMeshPoint.y - leftUpperMeshPoint.y;
        var zDeltaHeightLeft = leftUpperMeshPoint.y - leftLowerMeshPoint.y;
        var zDeltaHeightRight = rightUpperMeshPoint.y - rightLowerMeshPoint.y;


        var meshHeightPosition = leftLowerMeshPoint.y + zGradient * zDeltaHeightLeft;

        

        return meshHeightPosition;
    }


    private Vector3 ConvertToRelativePosition(Vector3 position)
    {
        var relativePosition = position - gameObject.transform.position;
        relativePosition.x /= gameObject.transform.localScale.x;
        relativePosition.z /= gameObject.transform.localScale.z;
        relativePosition += new Vector3(Dimension / 2f, 0, Dimension / 2f);

        return relativePosition;

    }

    private int index(int x, int z)
    {
        if (x < 0 || x > Dimension || z < 0 || z > Dimension)
        {
            Debug.LogWarning("Index tries access outside of dimension" + new Vector2(x, z));
            x = 0;
            z = 0;
        }

        return x * (Dimension + 1) + z;
    }

    private int[] GenerateTriangles()
    {

        var triangles = new int[mesh.vertices.Length * 6];
        for (var x = 0; x < Dimension; x++)
        {
            for (var z = 0; z < Dimension; z++)
            {
                triangles[index(x, z) * 6 + 0] = index(x, z);
                triangles[index(x, z) * 6 + 1] = index(x + 1, z + 1);
                triangles[index(x, z) * 6 + 2] = index(x + 1, z);
                triangles[index(x, z) * 6 + 3] = index(x, z);
                triangles[index(x, z) * 6 + 4] = index(x, z + 1);
                triangles[index(x, z) * 6 + 5] = index(x + 1, z + 1);
            }
        }

        return triangles;
    }

    void FixedUpdate()
    {
    }


    // Update is called once per frame
    void Update()
    {
        var vertices = mesh.vertices;

        for (var x = 0; x <= Dimension; x++)
        {
            for (var z = 0; z <= Dimension; z++)
            {
                var y = 0f;
                for (var o = 0; o < Octaves.Length; o++)
                {
                    if (!Octaves[o].active)
                    {
                        continue;
                    }
                    if (Octaves[o].alternate)
                    {
                        var perl = Mathf.PerlinNoise((x * Octaves[o].scale.x) / Dimension,
                            (z * Octaves[o].scale.y) / Dimension) * Mathf.PI * 2f;
                        y += Mathf.Cos(perl + Octaves[o].speed.magnitude * Time.time) * Octaves[o].height;

                    }
                    else
                    {
                        var perl = Mathf.PerlinNoise((x * Octaves[o].scale.x + Time.time * Octaves[o].speed.x) / Dimension,
                            (z * Octaves[o].scale.y + Time.time * Octaves[o].speed.y) / Dimension) -0.5f;
                        y += perl * Octaves[o].height;

                    }
                }
                vertices[index(x, z)] = MoveToCenter(new Vector3(x, y, z));
            }
        }

        mesh.vertices = vertices;
    }


    [Serializable]
    public struct Octave
    {
        public Vector2 speed;
        public Vector2 scale;
        public float height;
        public bool alternate;
        public bool active;
    }
}
