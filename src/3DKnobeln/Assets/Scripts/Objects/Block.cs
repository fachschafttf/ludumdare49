using UnityEngine;

// ReSharper disable once CheckNamespace
public class Block : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        // Don't do collision effect, if in water. Performance drops else
        if (transform.position.y < 0)
        {
            return;
        }
        var targets = UnityEngine.Object.FindObjectsOfType<GameObject>();
        for (int i = 0; i < targets.Length; ++i)
        {
            var receiver = targets[i].GetComponent<StressReceiver>();
            if (receiver == null) continue;
            float distance = Vector3.Distance(transform.position, targets[i].transform.position);
            /* Apply stress to the object, adjusted for the distance */
            receiver.InduceStress(0.04f + 0.03f * gameObject.GetComponent<Rigidbody>().velocity.magnitude);
        }
        foreach (ContactPoint contact in collision.contacts)
        {
            GameObject collisonEffect = Instantiate(Resources.Load("CollisionEffect") as GameObject, contact.point, Quaternion.identity);
            Color[] myColors = GetMyEmissionColors();
            Color newColor = myColors[0];
            foreach(Color color in myColors)
            {
                newColor = Color.Lerp(newColor, color, Random.value);
            }
            Material mat = new Material(collisonEffect.GetComponent<ParticleSystemRenderer>().material);
            mat.SetColor("_EmissionColor", newColor);
            mat.SetColor("_BaseColor", newColor);
            collisonEffect.GetComponent<ParticleSystemRenderer>().material = mat;
            collisonEffect.GetComponent<ParticleSystemRenderer>().trailMaterial = mat;

        }
    }


    public Color[] GetMyEmissionColors()
    {
        Material[] blockMats = gameObject.GetComponentInChildren<Renderer>().materials;
        Color[] colors = new Color[blockMats.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = blockMats[i].GetColor("_EmissionColor");
        }
        return colors;
    }

    public Vector3 HighestUnderlyingPoint()
    {
        Vector3 highestPoint = Vector3.zero;
        foreach (var coll in GetComponentsInChildren<BoxCollider>())
        {
            Bounds bounds = coll.bounds;


            LayerMask mask = LayerMask.GetMask("Environment", "Block");
            RaycastHit hit;
            Physics.BoxCast(bounds.center, bounds.extents, Vector3.down, out hit, Quaternion.identity, 400f, mask);
            if (hit.transform.gameObject == gameObject)
                continue;

            float upperPoint = hit.point.y;
            if (upperPoint > highestPoint.y)
                highestPoint = hit.point;

            // DrawLine(bounds.center, hit.point, Color.green);
        }
        return highestPoint;
    }

    public float DistanceToHighestUnderlyingPoint()
    {
        float minDistance = Mathf.Infinity;
        foreach (var coll in GetComponentsInChildren<BoxCollider>())
        {
            Bounds bounds = coll.bounds;


            LayerMask mask = LayerMask.GetMask("Environment", "Block");
            RaycastHit hit;


            if (Physics.BoxCast(bounds.center, bounds.extents, Vector3.down, out hit, Quaternion.identity, 400f, mask))
            {
                if (hit.transform.gameObject == gameObject)
                    continue;
                float distance = bounds.min.y - hit.point.y;
                if (distance < minDistance)
                    minDistance = distance;
                // DrawLine(bounds.center, hit.point, Color.red);

            }

            // DrawLine(bounds.center, hit.point, Color.green);
        }
        return minDistance;
    }

    // ReSharper disable once UnusedMember.Local
    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.05f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Universal Render Pipeline/Particles/Lit"));
        lr.startColor = lr.endColor = color;
        lr.startWidth = lr.endWidth = 0.1f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }

}
