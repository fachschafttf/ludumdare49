using UnityEngine;
using UnityEngine.InputSystem;

// ReSharper disable once CheckNamespace
public class MovementController : MonoBehaviour, PlayerActions.IPlayerControlsActions
{
    private enum Input { Left, Right, Up, Down }
    private PlayerActions _controls;
    private CameraController _cameras;
    private GameController _gameController;

    private Quaternion _targetRotation;

    public float RotationSpeed = 10f;

    private bool _buttonFeedback;
    [Space]
    [Header("UI Feedback: Movement")]
    [Tooltip("The UI button for Movement Left")]
    public AnimateButtonFeedback MoveLeftButton;
    [Tooltip("The UI button for Movement Right")]
    public AnimateButtonFeedback MoveRightButton;
    [Tooltip("The UI button for Movement Near")]
    public AnimateButtonFeedback MoveNearButton;
    [Tooltip("The UI button for Movement Far")]
    public AnimateButtonFeedback MoveFarButton;
    [Tooltip("The UI button for Movement Down")]
    public AnimateButtonFeedback MoveDownButton;
    [Space]
    [Header("UI Feedback: Rotation")]
    [Tooltip("The UI button for Rotation Down")]
    public AnimateButtonFeedback RotateDownButton;
    [Tooltip("The UI button for Rotation Up")]
    public AnimateButtonFeedback RotateUpButton;
    [Tooltip("The UI button for Rotation Left")]
    public AnimateButtonFeedback RotateLeftButton;
    [Tooltip("The UI button for Rotation Right")]
    public AnimateButtonFeedback RotateRightButton;

    [Space]
    [Header("strings of the sound clips")]
    public string SoundMoveLeft;
    public string SoundMoveRight;
    public string SoundMoveNear;
    public string SoundMoveFar;

    public string SoundRotateLeft;
    public string SoundRotateRight;
    public string SoundRotateDown;
    public string SoundRotateUp;

    public string SoundFastDown;



    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        _cameras = FindObjectOfType<CameraController>();
        if (_cameras == null)
        {
            Debug.Log("No CameraController in Scene");
        }

        _targetRotation = transform.rotation;
        _gameController = FindObjectOfType<GameController>();
        _buttonFeedback = TryToFindButtons();
        // Debug.Log($"button feedback for movement is set to {_buttonFeedback}!");
    }

    public void OnCameraRotation(InputAction.CallbackContext context)
    {
        // Prevent double execution.
        if (!context.performed || GameController.Instance?.Paused == true || GameController.Instance?.GameOver == true)
        {
            return;
        }
        var direction = context.ReadValue<Vector2>().normalized;
        if (direction.x > 0.5)
        {
            _cameras.WantRotateCamera(CameraController.SpinDirection.Left);
            if (_buttonFeedback)
            {
                _cameras.RotationLeftButton.PlayButtonPressAnimation();
            }
        }
        else if (direction.x < -0.5)
        {
            _cameras.WantRotateCamera(CameraController.SpinDirection.Right);
            if (_buttonFeedback)
            {
                _cameras.RotationRightButton.PlayButtonPressAnimation();
            }
        }
    }


    
    // Grab Movement input from input system.
    public void OnMovement(InputAction.CallbackContext context)
    {
        // Prevent double execution.
        if (!context.performed || GameController.Instance?.Paused == true || GameController.Instance?.GameOver == true)
        {
            return;
        }
        Vector2 direction = context.ReadValue<Vector2>().normalized;
        
        Input input = WhichInputIsThis(direction);
        MoveButtonFeedback(input);
        MoveButtonSound(input);

        direction = RotateToMatchCameraAngle(direction);
        MoveOnGrid(direction);

        //transform.Rotate(90 * GetRotationVectorViewRay() * rotationDirection, Space.World);
    }

    // ReSharper disable once UnusedMember.Local
    void Update()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, _targetRotation, RotationSpeed);
    }

    // Movement direction has to match current camera perspective. This projects the movement vector into the right perspective
    private Vector2 RotateToMatchCameraAngle(Vector2 direction)
    {
        var cameraDirection = _cameras ? _cameras.CurrentDirection : Directions.North;
        var rotation = (cameraDirection - Directions.North) * 90f;
        return Utilities.RotateVector2(direction, rotation);
    }

    // Move the object in the specified direction. Fixed on Grid.
    private void MoveOnGrid(Vector2 direction)
    {
        // TODO: Make movement on finer Grid possible.
        var gridMovement = Vector2Int.RoundToInt(direction);
        transform.position += new Vector3(gridMovement.x, 0, gridMovement.y);
    }

    // ReSharper disable once UnusedMember.Local
    public void RotateRandom()
    {
        var rotX = (Random.Range(0, 4)) * 90;
        var rotY = (Random.Range(0, 4)) * 90;
        var rotZ = (Random.Range(0, 4)) * 90;
        _targetRotation = Quaternion.Euler(new Vector3(rotX, rotY, rotZ));
    }
    public void OnTouchInput(InputAction.CallbackContext context)
    {
    }

    // LeftClick Action from Input System.
    public void OnAction(InputAction.CallbackContext context)
    {
        if (GameController.Instance?.Paused == true || GameController.Instance?.GameOver == true)
        {
            return;
        }

        if (context.phase == InputActionPhase.Started)
        {
            if (_buttonFeedback)
            {
                MoveDownButton.MakeBig();
            }
            StartFastDown();
        }
        else if (context.phase == InputActionPhase.Canceled)
        {
            StopFastDown();
            if (_buttonFeedback)
            {
                MoveDownButton.MakeSmall();
            }
        }
    }

    public void StartFastDown()
    {
        _gameController.StartMoveDown();
        AudioController.Instance.PlaySound(SoundFastDown);
    }

    public void StopFastDown()
    {
        _gameController.StopMoveDown();
        AudioController.Instance.StopSound(SoundFastDown);
    }

    // LeftClick Action from Input System.
    public void OnRotation(InputAction.CallbackContext context)
    {
        if (!context.performed || GameController.Instance?.Paused == true || GameController.Instance?.GameOver == true)
        {
            return;
        }
        Vector2 direction = context.ReadValue<Vector2>().normalized;

        // Get the dir:
        Input inputDirection = WhichInputIsThis(direction);
        RotateButtonFeedback(inputDirection);
        RotateButtonSound(inputDirection);

        switch (inputDirection)
        {
            case Input.Left:
                // Rotate Clockwise / Counter Clockwise in front of player
                RotateX(direction);
                break;
            case Input.Right:
                // Rotate Clockwise / Counter Clockwise in front of player
                RotateX(direction);
                break;
            case Input.Up:
                // Rotate towards / away from player.
                RotateY(direction);
                break;
            case Input.Down:
                // Rotate towards / away from player.
                RotateY(direction);
                break;
            default:
                break;
        }
    }

    private void RotateX(Vector2 direction)
    {
        var rotationDirection = direction.x > 0 ? -1 : 1;
        _targetRotation = Quaternion.Euler(90 * GetRotationVectorViewRay() * rotationDirection) * _targetRotation;
    }
    
    private void RotateY(Vector2 direction)
    {
        var rotationDirection = direction.y > 0 ? 1 : -1;
        _targetRotation = Quaternion.Euler(90 * GetRotationVectorViewRayOrthogonal() *  rotationDirection) * _targetRotation;
    }

    private Vector3 GetRotationVectorViewRay()
    {
        var viewDirection = _cameras.CurrentDirection;

        switch (viewDirection)
        {
            case Directions.North:
                return Vector3.forward;
            case Directions.East:
                return Vector3.right;
            case Directions.South:
                return Vector3.back;
            case Directions.West:
                return Vector3.left;
            default:
                return Vector3.zero;
        }
    }

    private Vector3 GetRotationVectorViewRayOrthogonal()
    {
        var viewDirection = _cameras.CurrentDirection;

        switch (viewDirection)
        {
            case Directions.North:
                return Vector3.right;
            case Directions.East:
                return Vector3.back;
            case Directions.South:
                return Vector3.left;
            case Directions.West:
                return Vector3.forward;
            default:
                return Vector3.zero;
        }
    }

    // helper
    private bool TryToFindButtons()
    {
        if (MoveLeftButton == null)
        {
            MoveLeftButton = GameObject.Find("UIElements/Movement/ButtonLeft").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (MoveRightButton == null)
        {
            MoveRightButton = GameObject.Find("UIElements/Movement/ButtonRight").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (MoveNearButton == null)
        {
            MoveNearButton = GameObject.Find("UIElements/Movement/ButtonNear").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (MoveFarButton == null)
        {
            MoveFarButton = GameObject.Find("UIElements/Movement/ButtonFar").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (MoveDownButton == null)
        {
            MoveDownButton = GameObject.Find("UIElements/ButtonBlockDown").GetComponentInChildren<AnimateButtonFeedback>();
        }

        if (RotateDownButton == null)
        {
            RotateDownButton = GameObject.Find("UIElements/Rotation/ButtonDown").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (RotateUpButton == null)
        {
            RotateUpButton = GameObject.Find("UIElements/Rotation/ButtonUp").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (RotateLeftButton == null)
        {
            RotateLeftButton = GameObject.Find("UIElements/Rotation/ButtonLeft").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (RotateRightButton == null)
        {
            RotateRightButton = GameObject.Find("UIElements/Rotation/ButtonRight").GetComponentInChildren<AnimateButtonFeedback>();
        }
        return (MoveLeftButton != null && MoveRightButton != null && MoveDownButton != null && RotateDownButton != null && RotateUpButton != null && RotateLeftButton != null && RotateRightButton != null);
    }


    private void MoveButtonSound(Input input)
    {
        string soundClip = input switch
        {
            Input.Left => SoundMoveLeft,
            Input.Right => SoundMoveRight,
            Input.Up => SoundMoveFar,
            Input.Down => SoundMoveNear,
            _ => "Error",
        };
        AudioController.Instance.PlaySound(soundClip);
    }

    private void RotateButtonSound(Input input)
    {
        string soundClip = input switch
        {
            Input.Left => SoundRotateLeft,
            Input.Right => SoundRotateRight,
            Input.Up => SoundRotateUp,
            Input.Down => SoundRotateDown,
            _ => "Error",
        };
        AudioController.Instance.PlaySound(soundClip);
    }
    private void MoveButtonFeedback(Input input)
    {
        if (!_buttonFeedback)
        {
            return;
        }

        switch (input)
        {
            case Input.Left:
                MoveLeftButton.PlayButtonPressAnimation();
                break;
            case Input.Right:
                MoveRightButton.PlayButtonPressAnimation();
                break;
            case Input.Up:
                MoveFarButton.PlayButtonPressAnimation();
                break;
            case Input.Down:
                MoveNearButton.PlayButtonPressAnimation();
                break;
            default:
                break;
        }
    }

    private void RotateButtonFeedback(Input input)
    {
        if (!_buttonFeedback)
        {
            return;
        }
        switch (input)
        {
            case Input.Left:
                RotateLeftButton.PlayButtonPressAnimation();
                break;
            case Input.Right:
                RotateRightButton.PlayButtonPressAnimation();
                break;
            case Input.Up:
                RotateUpButton.PlayButtonPressAnimation();
                break;
            case Input.Down:
                RotateDownButton.PlayButtonPressAnimation();
                break;
            default:
                break;
        }
    }

    private Input WhichInputIsThis(Vector2 direction)
    {
        Input inputDirection;
        if (direction.x != 0)
        {
            if (direction.x < 0)
            {
                inputDirection = Input.Left;
            }
            else
            {
                inputDirection = Input.Right;
            }
        }
        else
        {
            if (direction.y > 0)
            {
                inputDirection = Input.Up;
            }
            else
            {
                inputDirection = Input.Down;
            }
        }
        return inputDirection;
    }

    // UIElements functions
    public void MoveLeft()
    {
        var direction = RotateToMatchCameraAngle(Vector2.left);
        MoveOnGrid(direction);
    }
    
    public void MoveRight()
    {
        var direction = RotateToMatchCameraAngle(Vector2.right);
        MoveOnGrid(direction);
    }

    public void MoveNear()
    {
        var direction = RotateToMatchCameraAngle(Vector2.down);
        MoveOnGrid(direction);
    }

    public void MoveFar()
    {
        var direction = RotateToMatchCameraAngle(Vector2.up);
        MoveOnGrid(direction);
    }

    public void RotateLeft()
    {
        RotateX(Vector2.left);
    }

    public void RotateRight()
    {
        RotateX(Vector2.right);
    }
    
    public void RotateUp()
    {
        RotateY(Vector2.up);
    }
    
    public void RotateDown()
    {
        RotateY(Vector2.down);
    }
    
    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnEnable()
    {
        if (_controls == null)
        {
            _controls = new PlayerActions();
            _controls.PlayerControls.SetCallbacks(this);
        }
        _controls.Enable();
    }

    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnDisable()
    {
        _controls.Disable();
    }
}
