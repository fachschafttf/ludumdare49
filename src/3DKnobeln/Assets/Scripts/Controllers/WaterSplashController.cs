using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSplashController : MonoBehaviour
{
    public ParticleSystem WaterSplash;


    private Mesh _mesh;
    private Vector3[] _vertices;

    void Start()
    {
        CreateColliders();
    }

    private void CreateColliders()
    {
        _mesh = GetComponent<MeshFilter>().mesh;

        _vertices = _mesh.vertices;
        Debug.Log(_vertices.Length);
    }
}
