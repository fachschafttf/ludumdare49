using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ColorSetter : MonoBehaviour
{
    public Color TextColor = Color.white;

    // Start is called before the first frame update
    void Start()
    {
        
        var children = GetComponentsInChildren<TextMeshProUGUI>();
        foreach (var child in children)
        {
            child.color = TextColor;
        }
    }
}
