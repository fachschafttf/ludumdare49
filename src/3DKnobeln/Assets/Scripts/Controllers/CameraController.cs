using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

// ReSharper disable once CheckNamespace
public class CameraController : MonoBehaviour
{
    public enum SpinDirection { Left = 1, Right = -1 }
    public enum CameraMode { Stationary, SetToHighestBlock, FollowActive, Adaptive }

    [Tooltip("The perspective camera with the 'real' view")]
    public Camera CameraPerspective;
    [Space]
    public GameObject OrthographicCameraParent;
    [Tooltip("orthographic camera (looking in the direction: north = positive z/blue;  east = positive x/red;  south = negative z/blue;  west = negative x/red)")]
    public Camera CameraOrthographic;
    [Tooltip("orthographic camera looking from the top")]
    public Camera CameraMiniMap;
    [Tooltip("Perspective Camera travelling around")]
    public Camera GameOverCamera;
    [Space]
    [Tooltip("Everything Inside this collider should be captured by the orth. camera")]
    public BoxCollider ViewBox;

    [Tooltip("Time in seconds it should take to spin 90�"), Range(0.05f, 1f)]
    public float RotationSpeed;
    public Directions CurrentDirection => EstimateCurrentDirection();
   
    [Space]
    [Header("UI Feedback")]
    [Tooltip("The UI button for Rotation Left")]
    public AnimateButtonFeedback RotationLeftButton;
    [Tooltip("The UI button for Rotation Right")]
    public AnimateButtonFeedback RotationRightButton;

    [Space]
    [Header("strings of the sound clips for rotating the camera")]
    public string SoundCameraLeft;
    public string SoundCameraRight;

    [Space]
    [Tooltip("I just don't know what we want, so here's the option to choose")]
    public CameraMode Mode;

    /// <summary>
    /// returns the current viewing direction of the camera
    /// </summary>
    /// <returns></returns>
    private Directions EstimateCurrentDirection()
    {
        var rotation = CameraOrthographic.transform.rotation;
        var yValue = Utilities.Mod((int)rotation.eulerAngles.y, 360);
        var n = 0f;
        var e = 90f;
        var s = 180f;
        var w = 270f;
        if (yValue < (n + e) / 2f)
        {
            return Directions.North;
        }
        if (yValue < (e + s) / 2f)
        {
            return Directions.East;
        }
        if (yValue < (s + w) / 2f)
        {
            return Directions.South;
        }
        if (yValue < (w + 360f) / 2f)
        {
            return Directions.West;
        }
        return Directions.North;
    }

    // TODO: This is not used anymore and can maybe be removed, if approved by zachi
    // ReSharper disable once NotAccessedField.Local
    private Camera _currentCamera;
    private PlayerActions _controls;
    private GameController _gameController;


    private const float OrthDistance = 20f;

    private bool _cameraIsRotating;
    private List<SpinDirection> _spinQueue = new List<SpinDirection>();
    private bool _buttonFeedback;
    private GameObject mostRecentActiveBlock;


    // Start is called before the first frame update
    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        _currentCamera = CameraPerspective;
        AdaptCameraViewportsToMatchViewBox();
        UseOrthographicCamera();
        _buttonFeedback = TryToFindButtons();
        _gameController = FindObjectOfType<GameController>();
    }

    private bool TryToFindButtons()
    {
        if (RotationLeftButton == null)
        {
            RotationLeftButton = GameObject.Find("UIElements/CameraRotateLeft").GetComponentInChildren<AnimateButtonFeedback>();
        }
        if (RotationRightButton == null)
        {
            RotationRightButton = GameObject.Find("UIElements/CameraRotateRight").GetComponentInChildren<AnimateButtonFeedback>();
        }
        return (RotationLeftButton != null && RotationRightButton != null);
    }

    private void UseOrthographicCamera()
    {
        CameraPerspective.gameObject.SetActive(false);
        CameraOrthographic.gameObject.SetActive(true);
        _currentCamera = CameraOrthographic;
    }

    // ReSharper disable once UnusedMember.Local
    private void UsePerspectiveCamera()
    {
        FinishRotationQueueOfCamera();
        CameraOrthographic.gameObject.SetActive(false);
        CameraPerspective.gameObject.SetActive(true);
        _currentCamera = CameraPerspective;
    }

    private void FinishRotationQueueOfCamera()
    {
        // TODO clean up queue, so switching to perspective and back to ortographic while rotating does not bug it out
    }

    // ReSharper disable once UnusedMember.Local
    private void Update()
    {
    }

    // ReSharper disable once UnusedMember.Local
    private void FixedUpdate()
    {
        StartRotateCamera();
        AdaptCameraHeight();
    }



    private void StartRotateCamera()
    {
        if (_cameraIsRotating)
        {
            return;
        }

        if (_spinQueue.Count != 0)
        {
            var nextSpin = _spinQueue[0];
            string soundEffect = (int)nextSpin == 1 ? SoundCameraLeft : SoundCameraRight;
            AudioController.Instance.PlaySound(soundEffect);
            _spinQueue.RemoveAt(0);
            _cameraIsRotating = true;
            IEnumerator enumerator = RotateCamera90(nextSpin);
            StartCoroutine(enumerator);
        }
    }

    public void SwitchToGameOverCamera()
    {
        CameraOrthographic.gameObject.SetActive(false);
        GameOverCamera.gameObject.SetActive(true);
    }
    public void WantRotateCamera(SpinDirection spinD)
    {
        _spinQueue.Add(spinD);
    }

    IEnumerator RotateCamera90(SpinDirection spinDirection)
    {

        float necessaryAngle = 90f * (int)spinDirection;
        float anglePerSecond = necessaryAngle / RotationSpeed;
        float sumTime = 0f;
        
        while (sumTime < RotationSpeed)
        {
            float rotationThisFrame = Time.fixedDeltaTime * anglePerSecond;
            CameraOrthographic.transform.RotateAround(ViewBox.center, Vector3.up, rotationThisFrame);
            CameraMiniMap.transform.RotateAround(Vector3.zero, Vector3.up, rotationThisFrame);
            sumTime += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        // snap camera to next 90 deg step and fix position
        SnapFixCamera();
        _cameraIsRotating = false;
    }

    private void SnapFixCamera()
    {
        var dir = CurrentDirection;
        Vector3 pos = CameraOrthographic.transform.position;
        Vector3 euler = Vector3.zero;
        Vector3 eulerMiniMap = new Vector3(90, 0, 0);
        switch (dir)
        {
            case Directions.North:
                pos.x = 0f;
                pos.z = -OrthDistance;
                break;
            case Directions.East:
                pos.x = -OrthDistance;
                pos.z = 0f;
                euler.y = 90f;
                eulerMiniMap.y = 90f; 
                break;
            case Directions.South:
                pos.x = 0f;
                pos.z = OrthDistance;
                euler.y = 180f;
                eulerMiniMap.y = 180f;
                break;
            case Directions.West:
                pos.x = OrthDistance;
                pos.z = 0f;
                euler.y = 270f;
                eulerMiniMap.y = 270f;
                break;
            case Directions.Free:
                break;
        }
        CameraOrthographic.transform.rotation = Quaternion.Euler(euler);
        CameraMiniMap.transform.rotation = Quaternion.Euler(eulerMiniMap);
        CameraOrthographic.transform.position = pos;
    }

    private void AdaptCameraViewportsToMatchViewBox()
    {
        var center = ViewBox.center;
        OrthographicCameraParent.transform.position = center;

        var boxSize = Math.Max(ViewBox.bounds.size.x, Math.Max(ViewBox.bounds.size.y, ViewBox.bounds.size.z)) / 2f;
        CameraOrthographic.orthographicSize = boxSize;
        // Debug.Log($"{boxSize}");
    }

    /// <summary>
    /// IMPORTANT: uses parent of block, bc of crane
    /// </summary>
    /// <param name="block"></param>
    // ReSharper disable once UnusedMember.Local
    private void FollowActiveBlock()
    {
        GameObject block = _gameController.ActiveBlock;
        if (block == null)
        {
            return;
        }

        block = block.transform.parent.gameObject;

        FollowBlock(block);
    }

    private void FollowBlock(GameObject block)
    {
        var oldValues = OrthographicCameraParent.transform.position;
        var heightOffset = -CameraOrthographic.orthographicSize * 0.5f;

        // var newHeight = Math.Max(block.transform.parent.transform.position.y + heightOffset, oldValues.y);
        var newHeight = Math.Max(block.transform.position.y + heightOffset, CameraOrthographic.orthographicSize * .8f);
        OrthographicCameraParent.transform.position = new Vector3(
            oldValues.x,
            newHeight,
            oldValues.z);
    }

    private void AdaptCameraHeight()
    {
        switch (Mode)
        {
            case CameraMode.Stationary:
                return;

            case CameraMode.SetToHighestBlock:
                SetCameraToHighestBlock();
                break;
            case CameraMode.FollowActive:
                FollowActiveBlock();
                break;
            case CameraMode.Adaptive:
                AdaptiveCameraHeight();
                break;
            default:
                break;
        }
    }

    private void AdaptiveCameraHeight()
    {
        bool top = IsTopOfTowerNotInView();
        if (top)
        {
            SetCameraToHighestBlock();
        }
        bool followActiveBlock = ShouldFollowActiveBlockInstead();
        if (followActiveBlock)
        {
            FollowActiveBlock();
        }
        bool followMostRecentActiveBlock = ShouldFollowMostRecentActiveBlock();
        if (followMostRecentActiveBlock)
        {
            if (mostRecentActiveBlock != null)
            {
                FollowBlock(mostRecentActiveBlock);
            }
        }
    }

    private bool ShouldFollowMostRecentActiveBlock()
    {
        var activeBlock = _gameController.ActiveBlock;
        return activeBlock == null;
    }

    private bool ShouldFollowActiveBlockInstead()
    {
        var activeBlock = _gameController.ActiveBlock;
        if (activeBlock == null)
        {
            // we dont have an active block anymore!
            // no block, no problem
            return false;
        }
        var blockPos = activeBlock.transform.position;
        var highestPos = _gameController.GetHighestPosition().y;
        mostRecentActiveBlock = activeBlock;

        if (blockPos.y > highestPos)
        {
            // we dont care if it is above the top
            return false;
        }

        return true;
        //return activeBlock.y < ViewBox.center.y + ViewBox.bounds.size.y / 2f)

        float limitToBottomOfScreen = CameraOrthographic.orthographicSize * .8f;
        Vector3 posToTest = new Vector3(blockPos.x, blockPos.y + limitToBottomOfScreen, blockPos.z);
        return !ViewBox.bounds.Contains(posToTest);
    }

    private bool IsTopOfTowerNotInView()
    {
        var highest = _gameController.GetHighestPosition();
        float minDistanceToCeiling = ViewBox.bounds.size.y / 2f;
        return highest.y + minDistanceToCeiling > ViewBox.center.y + (ViewBox.bounds.size.y / 2f);
    }

    private void SetCameraToHighestBlock()
    {
        var oldValues = OrthographicCameraParent.transform.position;
        var highest = _gameController.GetHighestPosition();
        var newValue = new Vector3(oldValues.x,
            highest.y,
            oldValues.z
            );
        OrthographicCameraParent.transform.position = newValue;
    }
    
    // UIElements functions
    public void RotateCameraLeft()
    {
        WantRotateCamera(SpinDirection.Left);
    }
    
    public void RotateCameraRight()
    {
        WantRotateCamera(SpinDirection.Right);
    }
    
    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnEnable()
    {
        if (_controls == null)
        {
            _controls = new PlayerActions();
        }
        _controls.Enable();
    }

    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnDisable()
    {
        _controls.Disable();
    }
}
