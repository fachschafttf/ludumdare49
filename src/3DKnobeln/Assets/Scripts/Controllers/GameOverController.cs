using System.Collections;
using System.Collections.Generic;
using System.Xml.Xsl;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    public float RangeThreshold = 7f;
    public int BlockOutsideThreshold = 4;
    private float _tickInterval = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CheckGameOver", 0, _tickInterval);
    }

    private void CheckGameOver()
    {
        if (GameController.Instance.GameOver) return;
        var blocksInGame = GameController.Instance.AllBlocks;
        var blocksOutsideGrid = 0;
        foreach (var block in blocksInGame)
        {
            var blockPosition = block.transform.position;
            var gridPosition = new Vector2(blockPosition.x, blockPosition.z);
            if (Utilities.IsAnyCoordinateTooFar(gridPosition, RangeThreshold))
            {
                blocksOutsideGrid++;
            }

            // End game, if one block is below zero, or too many blocks are outside of gameField.
            if (blockPosition.y < 0 || blocksOutsideGrid > BlockOutsideThreshold)
            {
                GameController.Instance.SetGameOver();
                return;
            }
        }

    }
}
