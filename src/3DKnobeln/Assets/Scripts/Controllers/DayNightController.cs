using UnityEngine;

// ReSharper disable once CheckNamespace
public class DayNightController : MonoBehaviour
{
    public float DayLength;
    public Light Sun;
    public Light Moon;
    
    // Start is called before the first frame update
    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        Sun.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
        Moon.transform.rotation = Quaternion.Euler(new Vector3(270, 0, 0));

    }

    // Update is called once per frame
    // ReSharper disable once UnusedMember.Local
    void Update()
    {
        
    }

    // ReSharper disable once UnusedMember.Local
    void FixedUpdate()
    {
        Sun.transform.Rotate(Vector3.left, 1f / 360 * DayLength);
        Moon.transform.Rotate(Vector3.left, 1f / 360 * DayLength);
    }
}
