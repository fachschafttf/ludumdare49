using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;
using BoxCollider = UnityEngine.BoxCollider;
using GameObject = UnityEngine.GameObject;

public class WavesController : MonoBehaviour
{
    private Mesh _mesh;
    private Vector3[] _vertices;

    public float Scale = 8f;
    public float Speed = 0.1f;
    public float Height = 0.1f;
    public int ColliderRange = 25;

    public List<Tuple<GameObject, int>> Floaters = new List<Tuple<GameObject, int>>();

    void Start()
    {
        RetrieveMesh();
        CreateColliders();
    }

    private void CreateColliders()
    {

        var count = 0;
        for (var i = 0; i < _vertices.Length; i++)
        {
            if (Math.Abs(_vertices[i].x) <= ColliderRange/transform.localScale.x && Math.Abs(_vertices[i].z) <= ColliderRange / transform.localScale.z)
            {
                var newObj = new GameObject();
                newObj.transform.position = new Vector3(_vertices[i].x * transform.localScale.x, -30,  _vertices[i].z * transform.localScale.z);
                newObj.transform.parent = transform;
                var coll = newObj.AddComponent<BoxCollider>();
                coll.size = new Vector3(5f, 1f, 5f);

                Floaters.Add(new Tuple<GameObject, int>(newObj, i));
            }

        }

    }

    private void RetrieveMesh()
    {
        _mesh = GetComponent<MeshFilter>().mesh;
        _vertices = _mesh.vertices;
    }

    void FixedUpdate()
    {
        UpdateMesh();
        UpdateColliders();
    }


    private void UpdateMesh()
    {
        for (var i = 0; i < _vertices.Length; i++)
        {
            _vertices[i].y = Height * Mathf.PerlinNoise(Time.time * Speed + _vertices[i].x * Scale, Time.time * Speed + _vertices[i].z * Scale) - Height / 2;
        }

        _mesh.vertices = _vertices;
    }


    private void UpdateColliders()
    {
        foreach (var floater in Floaters)
        {
            var vertice = _vertices[floater.Item2];
            var position = floater.Item1.transform.position;
            position.y = vertice.y * transform.localScale.y + transform.parent.position.y - 2;
            floater.Item1.transform.position = position;
        }
    }
}
