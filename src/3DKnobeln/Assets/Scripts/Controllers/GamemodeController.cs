using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamemodeController : MonoBehaviour
{
    public static GamemodeController Instance;
    public int GameMode = 0;

    public static readonly string[] ModeList = {"adventure", "creative"};
    
    // ReSharper disable once UnusedMember.Local
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(gameObject);
        }
    }

}
