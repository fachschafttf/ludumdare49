using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBlockSpawner : MonoBehaviour
{
    public int SpawnRate = 1;
    void Start()
    {
        InvokeRepeating("SpawnRandomBlock", 0f, SpawnRate);
    }

    void SpawnRandomBlock()
    {
        var randomBlock =
            GameController.Instance.BlockPrefabs[Random.Range(0, GameController.Instance.BlockPrefabs.Count)];
        var spawnPosition = new Vector3(Random.Range(-5, 5), GameController.Instance.GetHighestPosition().y + 20,
            Random.Range(-5, 5));
        var newBlock = Instantiate(randomBlock, spawnPosition, Quaternion.identity);

        var rotX = (Random.Range(0, 4)) * 90;
        var rotY = (Random.Range(0, 4)) * 90;
        var rotZ = (Random.Range(0, 4)) * 90;
        newBlock.transform.Rotate(rotX, rotY, rotZ, Space.Self);
    }
}
