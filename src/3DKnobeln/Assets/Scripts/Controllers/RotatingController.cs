using UnityEngine;

public class RotatingController : MonoBehaviour
{
    public float Speed = 1f;
    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(Vector3.zero, Vector3.up, Speed * Time.deltaTime);
    }
}
