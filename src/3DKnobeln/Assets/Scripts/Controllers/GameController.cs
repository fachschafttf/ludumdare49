using System.Collections.Generic;

using TMPro;
using UnityEngine;

// ReSharper disable once CheckNamespace
public class GameController : MonoBehaviour
{
    public static GameController Instance;
    public GameObject BlockCrane;
    public List<GameObject> BlockPrefabs;
    public Vector3 StartPoint = new Vector3(0, 30, 0);
    public float DownwardsSpeed = 0.5f;
    public float RespawnCooldown = 1f;
    public int PlayerHighScore;
    public string PlayerName;
    
    public bool GameOver;

    private bool _paused;
    
    private GameObject _uiElements;
    


    public bool Paused
    {
        get => _paused;
        set
        { 
            _paused = value;
            AdjustGameTimeToPauseState();
        }
    }


    public GameObject ActiveBlock;
    public List<GameObject> AllBlocks = new List<GameObject>();
    private float _activeCooldown;
    private float _activeDownwardsSpeed = 1f;


    private TextMeshProUGUI _highScoreTmp;


    // ReSharper disable once UnusedMember.Local
    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        Time.timeScale = 1f;
        var highscore = GameObject.Find("Highscore");
        _uiElements = GameObject.Find("UIElements");
        if (highscore ? highscore : false)
        {
            _highScoreTmp = highscore.GetComponentInChildren<TextMeshProUGUI>();
        }
        // Creative Mode
        if (GamemodeController.Instance?.GameMode == 1)
        {
            DownwardsSpeed = 0;
        }
    }

    // Update is called once per frame
    // ReSharper disable once UnusedMember.Local
    void Update()
    {
        if (GameOver)
        {
            return;
        }
        UpdateHighScoreUI();

        if (ActiveBlock == null && _activeCooldown <= 0)
        {
            NewBlock();
        }
        else if (ActiveBlock == null)
            _activeCooldown -= Time.deltaTime;
    }

    // ReSharper disable once UnusedMember.Local
    private void LateUpdate()
    {

        BlockCrane.transform.position += Vector3.down * (Time.deltaTime * _activeDownwardsSpeed);

        /*
        if (blockCrane.transform.position.y < (GetHighestPosition() + dropDistance).y)
        {
            PlaceBlock();
            NewBlock();
        }
        */
    }


    private void AdjustGameTimeToPauseState()
    {
        Time.timeScale = _paused ? 0 : 1;
        _uiElements.SetActive(!_paused);
    }

    public void NewBlock()
    {
        GameObject nextBlock = BlockPrefabs[Mathf.RoundToInt(Random.Range(0, BlockPrefabs.Count))];
        BlockCrane.transform.position = StartPoint + new Vector3(0, GetHighestPosition().y, 0);
        //BlockCrane.transform.rotation = Quaternion.identity;
        SpawnBlock(nextBlock);
        _activeDownwardsSpeed = DownwardsSpeed;
        BlockCrane.GetComponent<MovementController>().RotateRandom();
    }

    // ReSharper disable once UnusedMember.Local
    private void RotateRandom(GameObject block)
    {
        var rotX = (Random.Range(0, 4)) * 90;
        var rotY = (Random.Range(0, 4)) * 90;
        var rotZ = (Random.Range(0, 4)) * 90;
        block.transform.Rotate(rotX, rotY, rotZ, Space.World);
    }

    private void SpawnBlock(GameObject prefab)
    {
        GameObject newBlock = Instantiate(prefab, BlockCrane.transform);
        newBlock.GetComponent<Rigidbody>().isKinematic = true;

        ActiveBlock = newBlock;
        ActiveBlock.AddComponent<FallingBlock>();
        ActiveBlock.GetComponent<FallingBlock>().GameController = this;
        

        //RotateRandom(ActiveBlock);

    }

    // ReSharper disable once InconsistentNaming
    private void UpdateHighScoreUI()
    {
        if (_highScoreTmp ? _highScoreTmp : false)
        {
            _highScoreTmp.SetText(PlayerHighScore.ToString());
        }
    }

    // ReSharper disable once UnusedMember.Local
    private void ResetHighScore()
    {
        PlayerHighScore = 0;
    }

    private void SetHighScore(int score)
    {
        PlayerHighScore = score;
    }
    
    public void PlaceBlock()
    {
        if (ActiveBlock == null)
            return;
        ActiveBlock.transform.parent = null;
        ActiveBlock.GetComponent<Rigidbody>().isKinematic = false;
        ActiveBlock.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ActiveBlock.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        ActiveBlock.name += " [" + AllBlocks.Count + "]";
        Destroy(ActiveBlock.GetComponent<FallingBlock>());
        AllBlocks.Add(ActiveBlock);
        ActiveBlock = null;
        _activeCooldown = RespawnCooldown;

        SetHighScore(PlayerHighScore + 10);

        AudioController.Instance.PlaySound((Utilities.Mod(AllBlocks.Count - 1, 7) + 1).ToString());

    }

    public void PlaceBlock(Vector3 shift)
    {
        BlockCrane.transform.position += shift;
        PlaceBlock();
    }

    public void StartMoveDown()
    {
        if (ActiveBlock == null)
        {
            return;
        }
        //ActiveBlock.GetComponent<FallingBlock>().SnapDown();
        _activeDownwardsSpeed = 20f;
      
    }

    public void StopMoveDown()
    {
        _activeDownwardsSpeed = DownwardsSpeed;
    }

    public GameObject GetHighestBlock()
    {
        if (AllBlocks.Count == 0)
            return null;
        GameObject highestBlock = AllBlocks[0];
        foreach(GameObject block in AllBlocks)
        {
            if (block.transform.position.y > highestBlock.transform.position.y)
                highestBlock = block;
        }

        return highestBlock;
    }

    public Vector3 GetHighestPosition()
    {
        GameObject block = GetHighestBlock();
        if (block == null)
            return Vector3.zero;
        return block.transform.position;
    }

    public void SetGameOver()
    {
        GameOver = true;
        var overlay = FindObjectOfType <Overlay>();
        overlay.GameOver();
        _uiElements.SetActive(false);
        Destroy(ActiveBlock);
        FindObjectOfType<CameraController>().SwitchToGameOverCamera();
        AudioController.Instance.StopSound("SoundFastDown");
    }
}
