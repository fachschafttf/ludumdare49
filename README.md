# LudumDare49

This is our project for the Ludum Dare 49 Gamejam. See htts://ldjam.com

## Playable demo

You can see the current version live at https://fachschafttf.gitlab.io/ludumdare49/

For more nightly builds see: [environments](https://gitlab.com/fachschafttf/ludumdare49/-/environments/folders/DEMO)

## Stable releases

For releases that are playable on Windows, Linux, etc. see [releases](https://gitlab.com/fachschafttf/ludumdare49/-/releases)
